WALK SCORE
----------

INTRODUCTION
-------------

Provides the Walk Score for node locations.
View http://www.walkscore.com/professional/ for more info on Walk Score.

Programmers can use the API to:
 * Integrate Walk Score into your site
 * Add Walk Score to your property listings
 * Enable searching and sorting by Walk Score

The Walk Score API is supported in the United States, Canada,
Australia, and New Zealand.


REQUIREMENTS
------------

This module requires the following modules:
 * GeoLocation (https://drupal.org/project/geolocation)


INSTALLATION
------------

 * Install as you would normally install a contributed drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/
   for further information.


CONFIGURATION
-------------

 * Go to /admin/config/services/walkscore and enter in your API key.
   Sign up at http://www.walkscore.com/professional/api-sign-up.php

	 On this page, you can also view a test result. If your API key is correct,
   you should get something similar to the following:

     Walk Score successfully returned.
     walkscore: 92
     description: Walker's Paradise
     updated: 2014-05-16 19:54:45.176110
     logo_url: http://cdn.walk.sc/images/api-logo.gif
     more_info_icon: http://cdn.walk.sc/images/api-more-info.gif
     more_info_link: http://www.walkscore.com/how-it-works.shtml
     ws_link: http://www.walkscore.com/score/8+hickory+st+w+waterloo/lat=43.4780345/lng=-80.525996/?utm_source=xxxxxxx.xxx&utm_medium=ws_api&utm_campaign=ws_api
     snapped_lat: 43.4775
     snapped_lon: -80.526

USAGE
-----

 * First, create a new field of type Walkscore Number
 * Next, when saving a node, get the walkscore number via the getWalkScore()
   function by passing in the geolocation data.
 * Finally, set the walkscore number to your field.

    if (\Drupal::moduleHandler()->moduleExists('walkscore')) {

      $location = [
        'lat' => $geo['lat'],
        'lon' => $geo['lng'],
      ];

      $node->field_walkscore = \Drupal\walkscore\WalkScore::getWalkScore($location);
    }

TROUBLESHOOTING
---------------

 * Check the Status Report and Recent Log Messages if you run into any errors.
 * Development API keys are limited to 5,000 calls per day.
   After 100 node inserts/updates you will receive a "Your daily API quota has
   been exceeded." message. Wait 24 hrs or sign up for a premium API key.


MAINTAINERS
-----------

Current maintainers:
  Mike Sage (sagesolutions) (http://drupal.org/user/2789573)
