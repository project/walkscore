<?php

namespace Drupal\walkscore\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\walkscore\Service\WalkScore;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * WalkScore Settings Form.
 */
class WalkScoreForm extends FormBase {

  /**
   * The WalkScore service.
   *
   * @var \Drupal\walkscore\Service\WalkScore
   */
  private WalkScore $walkScore;

  /**
   * Class constructor.
   */
  public function __construct(WalkScore $walkScore) {
    $this->walkScore = $walkScore;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('walkscore.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'walkscore_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $config = $this->config('walkscore.settings');

    $form['walkscore_api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('api_key'),
      '#description' => $this->t('Don\'t have a key? Sign up for one at <a href="@url">walkscore.com</a>.', ['@url' => 'https://www.walkscore.com/professional/api-sign-up.php']),
    ];

    $form['walkscore_test'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Test Results'),
      '#options' => [],
      '#description' => $this->walkScore->printTest(),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->messenger()
      ->addMessage($this->t("The WalkScore settings have been saved."));

    $this->configFactory->getEditable('walkscore.settings')
      ->set('api_key', $form_state->getValue("walkscore_api"))
      ->save();
  }

}
