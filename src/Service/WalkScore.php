<?php

namespace Drupal\walkscore\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * URL api for WalkScore.
 */
define('WALKSCORE_API_URL', 'https://api.walkscore.com/score');

/**
 * WalkScore class to retrieve text associated with walkscore number.
 */
class WalkScore {

  use StringTranslationTrait;

  /**
   * Drupal Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  private LoggerChannelInterface $logger;

  /**
   * The API Key.
   *
   * @var string
   */
  private string $apiKey;

  /**
   * The http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private ClientInterface $httpClient;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private RendererInterface $renderer;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger Factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The walkscore settings config.
   * @param \GuzzleHttp\ClientInterface $client
   *   The http client.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory, ClientInterface $client, RendererInterface $renderer) {
    $this->logger = $logger_factory->get('walkscore');
    $this->apiKey = $config_factory->get('walkscore.settings')->get('api_key');
    $this->httpClient = $client;
    $this->renderer = $renderer;
  }

  /**
   * Get walk score using API Key and location data (in array form).
   *
   * @param array $location
   *   The location field array.
   *
   * @return int
   *   A score between 0 - 100.
   */
  public function getWalkScore(array $location): int {

    $address = [];

    if (isset($location['street'])) {
      $address[] = $location['street'];
    }

    if (isset($location['city'])) {
      $address[] = $location['city'];
    }

    if (isset($location['province'])) {
      $address[] = $location['province'];
    }

    $query = [
      'wsapikey' => $this->apiKey,
      'address' => urlencode(implode(",", $address)),
      'lat' => $location['lat'],
      'lon' => $location['lon'],
      'format' => "json",
    ];

    $url = WALKSCORE_API_URL . "?" . http_build_query($query);

    return $this->sendRequest($url);

  }

  /**
   * Sends request to url to get the walkscore.
   *
   * @param string $url
   *   The url.
   *
   * @return int
   *   The walk score number.
   */
  private function sendRequest(string $url): int {

    try {
      $request = $this->httpClient->request('GET', $url);

      if ($request->getStatusCode() != 200) {
        return 0;
      }
      else {
        $response = json_decode($request->getBody()->getContents(), TRUE);
        if ($response['status'] == 1) {
          return $response['walkscore'];
        }
        else {
          $this->logger->error($this->t("Error calculating Walk Score.") . $this->getStatusCodeDescription($response['status']));
          return 0;
        }
      }
    }
    catch (GuzzleException $e) {
      $this->logger->error($e->getMessage());
      return 0;
    }

  }

  /**
   * Map the API codes to user-friendly text.
   *
   * @param int $code
   *   The error/status code.
   *
   * @return string
   *   The Code description
   */
  public function getStatusCodeDescription(int $code): string {
    $responses = [
      '1' => $this->t('Walk Score successfully returned.'),
      '2' => $this->t('Score is being calculated and is not currently available.'),
      '30' => $this->t('Invalid latitude/longitude.'),
      '31' => $this->t('Walk Score API internal error.'),
      '40' => $this->t('Your WSAPIKEY is invalid.'),
      '41' => $this->t('Your daily API quota has been exceeded.'),
      '42' => $this->t('Your IP address has been blocked.'),
    ];

    return $responses[$code] ?? $this->t('Error :code. Code not found.', [":code" => $code]);
  }

  /**
   * Returns a user-friendly description from the score.
   *
   * @param int $score
   *   The score value.
   *
   * @return string
   *   The score description.
   *
   * @see http://www.walkscore.com/methodology.shtml
   */
  public function getScoreDescription(int $score): string {
    $description = "";

    switch (TRUE) {
      case $score == 0:
        $description = $this->t("No Data");
        break;

      case $score < 50:
        $description = $this->t("Car-Dependent");
        break;

      case $score < 70:
        $description = $this->t("Somewhat Walkable");
        break;

      case $score < 90:
        $description = $this->t("Very Walkable");
        break;

      case $score <= 100:
        $description = $this->t("Walker's Paradise");
        break;
    }

    return $description;
  }

  /**
   * Returns a user-friendly full description from the score.
   *
   * @param int $score
   *   The score value.
   *
   * @return string
   *   The score description.
   *
   * @see http://www.walkscore.com/methodology.shtml
   */
  public function getScoreDescriptionDetail(int $score): string {
    $description = "";

    switch (TRUE) {
      case $score == 0:
        $description = $this->t("Walk Score Error");
        break;

      case $score < 25:
        $description = $this->t("Almost all errands require a car.");
        break;

      case $score < 50:
        $description = $this->t("Most errands require a car.");
        break;

      case $score < 70:
        $description = $this->t("Some errands can be accomplished on foot.");
        break;

      case $score < 90:
        $description = $this->t("Most errands can be accomplished on foot.");
        break;

      case $score <= 100:
        $description = $this->t("Daily errands do not require a car.");
        break;
    }
    return $description;
  }

  /**
   * Print out a sample WalkScore.
   *
   * @return string
   *   html output of an example WalkScore.
   */
  public function printTest(): string {

    $location = [
      'street' => "8 hickory st w",
      'city' => 'waterloo',
      'province' => 'ontario',
      'lat' => '43.4780345',
      'lon' => '-80.525996',
      'format' => "json",
    ];

    $score = $this->getWalkScore($location);

    $output = "<div>Test Location: ";
    $output .= $location['street'] . ", " . $location['city'] . ", " . $location['province'] . "</div>";

    $renderable = [
      '#theme' => 'walkscore',
      '#score' => $score,
      '#description' => $this->getScoreDescription($score),
      '#long_description' => $this->getScoreDescriptionDetail($score),
    ];
    return $output . $this->renderer->renderPlain($renderable);

  }

}
