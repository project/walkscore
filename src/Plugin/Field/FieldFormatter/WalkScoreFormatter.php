<?php

namespace Drupal\walkscore\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\NumericFormatterBase;

/**
 * Plugin implementation of the 'WalkScore_default' formatter.
 *
 * @FieldFormatter(
 *   id = "WalkScore_default",
 *   label = @Translation("WalkScore text"),
 *   field_types = {
 *     "walkscore"
 *   }
 * )
 */
class WalkScoreFormatter extends NumericFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $summary = [];

    $summary[] = $this->t('Displays the score and description.');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {

    $elements = [];

    foreach ($items as $delta => $item) {
      $score = $this->numberFormat($item->value);

      $walkscore = \Drupal::service('walkscore.service');

      $elements[$delta] = [
        '#theme' => 'walkscore',
        '#score' => $score,
        '#description' => $walkscore->getScoreDescription($score),
        '#long_description' => $walkscore->getScoreDescriptionDetail($score),
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function numberFormat($number): string {
    return floor($number);
  }

}
