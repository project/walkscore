<?php

namespace Drupal\walkscore\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\NumberWidget;

/**
 * Provides a number widget.
 *
 * @FieldWidget(
 *   id = "walkscore",
 *   label = @Translation("WalkScore Widget"),
 *   description = @Translation("WalkScore description."),
 *   field_types = {
 *     "walkscore"
 *   }
 * )
 */
class WalkScoreWidget extends NumberWidget {

}
