<?php

namespace Drupal\walkscore\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\IntegerItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a field type of walkscore.
 *
 * @FieldType(
 *  id = "walkscore",
 *  label = @Translation("WalkScore Number (integer)"),
 *  description = @Translation("This field stores a WalkScore in the database
 *   as an integer."), category = @Translation("Number"), default_widget =
 *   "number", default_formatter = "number_integer"
 * )
 */
class WalkScoreItem extends IntegerItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings(): array {
    return [
      'min' => '0',
      'max' => '100',
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {
    $element = [];
    $settings = $this->getSettings();

    $element['min'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimum'),
      '#default_value' => $settings['min'],
      '#description' => $this->t('The minimum value that should be allowed in this field. Leave blank for no minimum.'),
    ];
    $element['max'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum'),
      '#default_value' => $settings['max'],
      '#description' => $this->t('The maximum value that should be allowed in this field. Leave blank for no maximum.'),
    ];
    return $element;
  }

}
